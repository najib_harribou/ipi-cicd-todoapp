import addFullName from '../../src/helpers/add-full-name';

describe('addFullName', () => {
  it('should add a fullName property to a person object', () => {
    // ARRANGE
    const person = {
      firstName: 'John',
      lastName: 'Doe',
    };

    // ACT
    const result = addFullName(person);

    // ASSERT
    expect(result).toEqual({
      firstName: 'John',
      lastName: 'Doe',
      fullName: 'John Doe',
    });
  });
});
