import castDoneToBoolean from '../../src/helpers/cast-done-to-boolean';

describe('castDoneToBoolean', () => {
  it('should return an item with the done property set to false when it is 0', () => {
    const item = {
      id: 1,
      name: 'Écrire un pipeline CI',
      done: 0,
      createdAt: '2021-09-01T12:00:00.000Z',
      updatedAt: '2021-09-01T12:00:00.000Z',
    };

    const result = castDoneToBoolean(item);

    expect(result).toEqual({
      id: 1,
      name: 'Écrire un pipeline CI',
      done: false,
      createdAt: '2021-09-01T12:00:00.000Z',
      updatedAt: '2021-09-01T12:00:00.000Z',
    });
  });

  it('should return an item with the done property set to true when it is 1', () => {
    const item = {
      id: 1,
      name: 'Écrire un pipeline CI',
      done: 1,
      createdAt: '2021-09-01T12:00:00.000Z',
      updatedAt: '2021-09-01T12:00:00.000Z',
    };

    const result = castDoneToBoolean(item);

    expect(result).toEqual({
      id: 1,
      name: 'Écrire un pipeline CI',
      done: true,
      createdAt: '2021-09-01T12:00:00.000Z',
      updatedAt: '2021-09-01T12:00:00.000Z',
    });
  });
});

