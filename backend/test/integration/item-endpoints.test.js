import request from 'supertest';
import app from '../../src/app';
import { pool } from '../../src/db';
import { createItems, resetItemsTable } from '../helpers/item';
import { createItem } from '../../src/models/item';

describe('/api/items endpoints', () => {
  // Effacement de la table avant les tests
  beforeAll(async () => {
    await resetItemsTable();
  });

  // Fermeture de la connexion à la BDD après les tests
  afterAll(async () => {
    pool.end();
  });

  // Tests de la route GET /api/items
  describe('GET /api/items', () => {
    it('should return some items', async () => {
      // ARRANGE
      // Création de 4 éléments
      await createItems([
        'Write tests',
        'Run tests',
        'Refactor code',
        'Run tests again',
      ]);

      // ACT
      // Récupération des éléments
      const res = await request(app).get('/api/items');

      // ASSERT
      // Vérification du code de statut et de la réponse
      expect(res.statusCode).toEqual(200);
      // Vérification du nombre d'éléments
      expect(res.body).toHaveLength(4);
      // Vérification de la forme de la réponse
      expect(res.body).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            id: expect.any(Number),
            name: expect.any(String),
            done: expect.any(Boolean),
            createdAt: expect.any(String),
            updatedAt: expect.any(String),
          }),
        ])
      );
    });
  });

  // Tests de la route POST /api/items
  describe('POST /api/items', () => {
    it('should fail to add an item if no name is sent', async () => {
      // ARRANGE
      const payload = {};

      // ACT
      const res = await request(app).post('/api/items').send(payload);

      // ASSERT
      expect(res.statusCode).toEqual(400);
      expect(res.body).toEqual({
        message: 'Name is required',
      });
    });

    it('should add an item', async () => {
      const item = {
        name: 'Write tests',
      };

      const res = await request(app).post('/api/items').send(item);

      expect(res.statusCode).toEqual(201);
      expect(res.body).toEqual(
        expect.objectContaining({
          id: expect.any(Number),
          name: 'Write tests',
          done: false,
          createdAt: expect.any(String),
          updatedAt: expect.any(String),
        })
      );
    });
  });

  // Tests de la route PUT /api/items/:id
  describe('PUT /api/items/:id', () => {
    it('should update an item', async () => {
      // ARRANGE
      const newItem = await createItem('Write tests');
      const { id } = newItem;
      const name = 'Write more tests';
      const done = true;

      // ACT
      const res = await request(app)
        .put(`/api/items/${id}`)
        .send({ name, done });

      // ASSERT
      expect(res.statusCode).toEqual(200);
      expect(res.body).toEqual(
        expect.objectContaining({
          id,
          name,
          done,
          createdAt: expect.any(String),
          updatedAt: expect.any(String),
        })
      );
    });

    it('should fail to update an item if no name is sent', async () => {
      const newItem = await createItem('Write tests');
      const { id } = newItem;

      const res = await request(app).put(`/api/items/${id}`).send();

      expect(res.statusCode).toEqual(400);
      expect(res.body).toEqual({
        message: 'Name or done is required',
      });
    });
  });

  describe('DELETE /api/items/:id', () => {
    it('should delete an item', async () => {
      const newItem = await createItem('Write tests');
      const { id } = newItem;

      const res = await request(app).delete(`/api/items/${id}`);

      expect(res.statusCode).toEqual(204);
    });
  });
});
